let employees = {
    "154636": {
        "first_name": "Chinh",
        "last_name" : "Le",
        "birth_date" : "14/07/1999",
        "picture" : '../pic/account.png',
        "sex" :  "m",
        "role" : "security"
    },
    "222345": {
        "first_name": "Khuong Duy",
        "last_name" : "Pham",
        "birth_date" : "31/01/1998",
        "picture" : '../pic/account.png',
        "sex": "m",
        "role" : "manager"
    },
    "386723": {
        "first_name": "Ariel",
        "last_name" : "Kovler",
        "birth_date" : "08/09/1991",
        "picture" : '../pic/account.png',
        "sex": "m",
        "role": "cashier"
    },
    "902372": {
        "first_name": "Nguyen",
        "last_name" : "Minh Chien",
        "birth_date" : "23/12/1998",
        "picture" : '../pic/account.png',
        "sex": "m",
        "role": "seller"
    },
    "912847": {
        "first_name": "Linh",
        "last_name" : "Linh",
        "birth_date" : "06/06/1999",
        "picture" : '../pic/account.png',
        "sex": "f",
        "role": "seller"
    }
};


for (let employee in employees){
    let list_item = $("<li class='emp' value=" + employee +">" + employees[employee]["first_name"] + " " + employees[employee]["last_name"] + "</li>");
    let list = $(".content ul");
    list.append(list_item);
}


$(".emp").click(function () {
    let id = $(this)[0].value;
    let container  = $("#card");
    container.empty();
    let card = $("<div class='account'></div>");
    let name = $("<span>Name: " + employees[id].first_name + " " + employees[id].last_name + "</span>");
    let date = $("<span>Birth date: " + employees[id].birth_date + "</span>");
    let role = $("<span>Role: " + employees[id].role + "</span>");
    let pic = $("<img src='./pics/account.png'>");
    card.append(pic);
    card.append(name);
    card.append(date);
    card.append(role);
    container.append(card);
});