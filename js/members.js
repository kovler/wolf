let members = {
    "154636": {
        "first_name": "jack",
        "last_name" : "johnson",
        "activities": [
            {
                "type" : "buy",
                'title': '1984',
                'date': '30/4/2019'
            }
        ]
    },
    "365434": {
        "first_name": "johny",
        "last_name" : "bravo",
        "activities": [
            {
                "type" : "rent",
                'title': 'The Old Man And The Sea',
                'date': '10/1/2019',
                'date_to_return' : '10/1/2020'
            },
            {
                "type" : "rent",
                'title': 'The Martian',
                'date': '10/1/2019',
                'date_to_return' : '10/1/2020'
            },
            {
                "type" : "buy",
                'title': 'Inferno',
                'date': '22/3/2019',
            }
        ]
    },
    "657457": {
        "first_name": "hoturi",
        "last_name" : "hanzo",
        "activities": [
            {
                "type" : "buy",
                'title': 'Dark Places',
                'date': '2/4/2019'
            }
        ]
    },
    "546454": {
        "first_name": "james",
        "last_name" : "dean",
        "activities": [
            {
                "type" : "buy",
                'title': 'Birdsong',
                'date': '16/8/2019'
            },
            {
                "type" : "buy",
                'title': 'Life of Pi',
                'date': '1/6/2019'
            },
            {
                "type" : "rent",
                'title': 'A Brief History of Time',
                'date': '27/9/2019',
                'date_to_return' : '27/12/2019'
            }
        ]
    },
    "546745": {
        "first_name": "Joan",
        "last_name" : "Baez",
        "activities": [
            {
                "type" : "buy",
                'title': 'American Psycho',
                'date': '13/7/2019'
            }
        ]
    },
};


for (let member in members){
    let list_item = $("<li class='emp' value=" + member +">" + members[member]["first_name"] + " " + members[member]["last_name"] + "</li>");
    let list = $(".content ul");
    list.append(list_item);
}


$(".emp").click(function () {
    let id = $(this)[0].value;
    let table  = $(".content table");
    table.empty();
    let headers = $("<tr> <th>type</th> <th>title</th> <th>date</th> <th>date to return</th> </tr>");
    table.append(headers);
    for (let i = 0; i < members[id].activities.length; i++){
        let activity = members[id].activities[i];
        if(activity.type === 'buy'){
            let row = $("<tr> <td>" + activity.type + "</td> <td>" + activity.title + "</td> <td>" + activity.date + "</td> <td></td> </tr>");
            table.append(row);
        }
        else{
            let row = $("<tr> <td>" + activity.type + "</td> <td>" + activity.title + "</td> <td>" + activity.date + "</td> <td>" + activity.date_to_return + "</td> </tr>");
            table.append(row);
        }

    }
});