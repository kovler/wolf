let db = {
    "books": {
        "Management Information Systems": {
            "year": 2019,
            "author": "Kenneth C. Laudon",
            "storage": 5,
            "shelf": 5,
            "rent": 5,
            "genre": "Educational"
        },
        "The Old Man And The Sea": {
            "year": 1960,
            "author": "Ernest Hemminguay",
            "storage": 7,
            "shelf": 3,
            "rent": 5,
            "genre": "Adventure",
            "publisher": "Charles Scribner\'s Sons"
        },
        "1984": {
            "year": 1949,
            "author": "George Orwell",
            "storage": 2,
            "shelf": 10,
            "rent": 3,
            "genre": "Dystopian fiction",
            "publisher": "Secker & Warburg"
        },
        "The Animal Farm": {
            "year": 1945,
            "author": "George Orwell",
            "storage": 20,
            "shelf": 5,
            "rent": 0,
            "genre": "Dystopian Fiction",
            "publisher": "Secker & Warburg"
        },
        "Gone Girl": {
            "year": 2012,
            "author": "Gillian Flynn",
            "storage": 30,
            "shelf": 5,
            "rent": 1,
            "genre": "Thriller"
        },
        "Dark Places": {
            "year": 2009,
            "author": "Gillian Flynn",
            "storage": 7,
            "shelf": 5,
            "rent": 2,
            "genre": "Mystery"
        },
        "The Martian": {
            "year": 2011,
            "author": "Andy Weir",
            "storage": 10,
            "shelf": 5,
            "rent": 0,
            "genre": "Science Fiction"
        },
        "Inferno": {
            "year": 1320,
            "author": "Dante Alighieri",
            "storage": 12,
            "shelf": 4,
            "rent": 1,
            "genre": "Poetry"
        },
        "Birdsong": {
            "year": 1993,
            "author": "Sebastian Faulks",
            "storage": 20,
            "shelf": 5,
            "rent": 0,
            "genre": "Historical"
        },
        "Life of Pi": {
            "year": 2001,
            "author": "Yann Martel",
            "storage": 10,
            "shelf": 10,
            "rent": 4,
            "genre": "Adventure"
        },
        "The Girl With the Dragon Tattoo": {
            "year": 2005,
            "author": "Steig Larsson",
            "storage": 17,
            "shelf": 4,
            "rent": 2,
            "genre": "Mystery"
        },
        "A Brief History of Time": {
            "year": 1988,
            "author": "Stephan Hawking",
            "storage": 17,
            "shelf": 4,
            "rent": 0,
            "genre": "Science"
        },
        "One Flew Over The Cuckoos Nest": {
            "year": 1962,
            "author": "Ken Kasey",
            "storage": 2,
            "shelf": 4,
            "rent": 0,
            "genre": "Drama"
        },
        "American Psycho": {
            "year": 1991,
            "author": "Bret Easton Ellis",
            "storage": 1,
            "shelf": 3,
            "rent": 1,
            "genre": "Horror"
        },
        "The Great Gatsby": {
            "year": 1925,
            "author": "F.Scott Fitzgerald",
            "storage": 8,
            "shelf": 3,
            "rent": 1,
            "genre": "Tragedy"
        }
    }
};


let books_list = $("#books_list");
for (let book in db.books) {
    let div = $("<div class='book_item'></div>");
    let genre = db.books[book].genre.toLocaleLowerCase();
    let input = $("<input name='books' value='" + book + "' genre='" + genre + "' type='radio'>");
    div.append(input);
    div.append(book);
    if (book === "American Psycho"){
        let noti = $("<div class=\"noti\"> 1</div><br>");
        div.append(noti);
    }
    books_list.append(div);
}



$("input[name='filter']").click(function () {
    let selected_genre = $("input[name='filter']:checked")[0].value;
    if (selected_genre != "all_books") {
        let all_books = $("div.book_item");
        all_books.css("display", 'none');
        for (let i = 0; i < all_books.length; i++) {
            if (all_books[i].children[0].getAttribute("genre") === selected_genre) {
                $(all_books[i]).css("display", "block");
            }
        }
    } else {
        let all_books = $("div.book_item");
        all_books.css("display", 'none');
        all_books.css("display", 'block');
    }
});

$("input[name='books']").click(function () {
    let selected_book = $("input[name='books']:checked")[0].value;
    let book_details = db.books[selected_book];
    $('span[name="book_title"]').text(selected_book);
    $('input[name="rented"]').val(book_details["rent"]);
    $('input[name="shelf"]').val(book_details["shelf"]);
    $('input[name="storage"]').val(book_details["storage"]);
    $('span[name="total"]').text(book_details["shelf"] + book_details["rent"] + book_details["storage"]);

    if (book_details["storage"] + book_details["shelf"] < 10) {
        $('p[name="notice"]').css('display', 'block');
    } else {
        $('p[name="notice"]').css('display', 'none');
    }
});

$(".add_book > button").click(function () {
    $('#add_book_modal').css('display', 'block');
});

$("#add_book_button").click(function () {
    let title = $('#add_book_modal input[name="title"]').val();
    let author = $('#add_book_modal input[name="author"]').val();
    let genre = $('#add_book_modal input[name="genre"]').val();
    let year = $('#add_book_modal input[name="year"]').val();
    let book_obj = {
        "author": author,
        "year": year,
        "genre": genre,
        "shelf": 0,
        "rent": 0,
        "storage": 0
    };
    db.books[title] = book_obj;
    location.reload();
});

$(".close_modal").click(function () {
    $('#add_book_modal').css('display', 'none');
});